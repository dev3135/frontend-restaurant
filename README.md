# Frontend restaurant

## Environnement de production (HTTP uniquement)

L'application frontend se trouve à l'adresse suivante :

[Quai Antique Frontend PROD](http://quai-antique-front.herokuapp.com/)

Connexion interface gestionnaire

[Quai Antique Backend PROD](http://quai-antique-alex.herokuapp.com/)


## Prérequis Déploiement en local

Avoir installé la partie backend pour avoir les données (fixtures)

[Déploiement en local du backend](https://gitlab.com/dev3135/backend-restaurant)

## Note importante 

Copier le fichier .env.local.exemple en .env.local

```
cp .env.local.exemple .env.local
```

L'url API se trouve dans le fichier :

```
/src/api/axios.js
```

## Installer NPM

```
npm install -g npm@9.6.2
```

## Installer Yarn

```
sudo npm install --global yarn
```

## Installer react scripts

```
yarn add react-scripts
```

## Démarrer l'application

```
yarn start
```

