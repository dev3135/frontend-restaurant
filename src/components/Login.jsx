import { useRef, useState, useEffect, useContext } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import PersonIcon from '@mui/icons-material/Person';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import axios from '../api/axios';
import Alert from '@mui/material/Alert';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Header from './Header';
import { AppBar, Toolbar } from '@mui/material';
import Footer from './Footer';

const theme = createTheme({
    palette: {
      primary: {
        main: '#B6AC97',
      },
      secondary: {
        main: '#392C1E',
      },
      secondary2: {
        main: '#906427',
      }
    },
});


const LOGIN_URL = '/api/login_check';
const USER_URL = '/api/users';

const Login = ({ setToken, setAccount }) => {
    const [username] = useState('');
    const [password] = useState('');
    const [error, setError] = useState(false)
    const [errMsg, setErrMsg] = useState('');

    useEffect(() => {
        setErrMsg('');
    }, [username, password]);

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const data = new FormData(event.currentTarget);
            const username = data.get('email');
            const password = data.get('password');
            const response = await axios.post(
                LOGIN_URL,
                JSON.stringify({ username, password }),
                {
                    headers: { 'Content-Type': 'application/json' },
                }
            );
            const token = response?.data?.token;
            const headers = { 
                'Authorization': `Bearer ${token}`
            }
            const responseAccount = await axios.get(
                `${USER_URL}/email/${username}`,
                {
                    headers
                }
            );
            const account = responseAccount?.data;
            setAccount({account});
            setToken({token});
        } catch (err) {
            if (!err?.response) {
                setErrMsg('Aucune réponse du serveur');
            } else if (err.response?.status === 400) {
                setErrMsg('Mauvais email ou mot de passe');
            } else if (err.response?.status === 401) {
                setErrMsg('Accès non autorisé');
            } else {
                setErrMsg('Echec authentification');
            }
            setError(true)
        }
    };

    return (
            <ThemeProvider theme={theme}>
            <CssBaseline />
            <Header/>
            <AppBar position="relative">
            <Toolbar>
            <PersonIcon sx={{ mr: 2 }} />
            <Typography variant="h4" color="inherit" noWrap>
              Connexion
            </Typography>
            </Toolbar>
            </AppBar>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Authentification
                    </Typography>
                    {error && (
                        <Box sx={{ display: 'flex', alignItems: 'center', m: 4}}>
                            <Alert severity="warning">{errMsg}</Alert>
                        </Box>
                    )}
                    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Addresse email"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Mot de passe"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Se souvenir de moi"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Se connecter
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Link href="#" variant="body2" color="inherit">
                                    Mot de passe oublié ?
                                </Link>
                            </Grid>
                            <Grid item>
                                <Button href="/creer-compte" variant="contained" color='secondary'>
                                    {"Créer un compte"}
                                </Button>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
            <Footer/>
        </ThemeProvider>
    );
};

export default Login;