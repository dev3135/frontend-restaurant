import * as React from 'react';
import logo from '../../assets/logo-quai-antique2.png'
import '../../styles/Header.css'
import { Link } from 'react-router-dom'
import useAccount from '../userAccount';
import Button from '@mui/material/Button';

function Header() {
    const { account } = useAccount();
    const title = 'Quai Antique';
    return (
        <React.Fragment>
            <header>
                <div className='lmj-banner'>
                    <a href='/' title={title}><img src={logo} alt={title}/></a>
                </div>
                <nav> 
                    <Link to="/">Accueil</Link>
                    <Link to="/carte">Carte et Menus</Link>
                    {account && (
                        <Button href="/mon-compte" variant="contained" color='secondary'>{account.prenom} {account.nom} </Button>
                    )}
                    {!account && (
                        <Button href="/connexion" variant="contained" color='secondary' >Se connecter</Button>
                    )}
                </nav>
            </header>
        </React.Fragment>
    )
}

export default Header