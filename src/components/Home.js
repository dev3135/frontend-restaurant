import * as React from 'react';
import Footer from './Footer';
import Galerie from './Galerie';
import Header from './Header';
import { ThemeProvider } from '@mui/material/styles';
import { createTheme, CssBaseline } from '@mui/material';

const theme = createTheme({
  palette: {
    primary: {
      main: '#B6AC97',
    },
    secondary: {
      main: '#392C1E',
    },
    tertiary: {
      main: '#906427',
    }
  },
});

function Home() {
  return (
      <React.Fragment>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Header />
          <Galerie/>
          <Footer/>
        </ThemeProvider>
      </React.Fragment>
  )
}

export default Home
