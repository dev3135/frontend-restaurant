import * as React from 'react';
import Carte from './Carte';
import Footer from './Footer';
import Header from './Header';
import { ThemeProvider } from '@mui/material/styles';
import { createTheme, CssBaseline } from '@mui/material';
import {  AppBar, Toolbar, Typography } from "@mui/material"
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';

const theme = createTheme({
    palette: {
      primary: {
        main: '#B6AC97',
      },
      secondary: {
        main: '#392C1E',
      },
      tertiary: {
        main: '#906427',
      }
    },
  });

function CartePage(){
    return(
        <React.Fragment>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Header/>
                <AppBar position="relative">
                  <Toolbar>
                  <RestaurantMenuIcon sx={{ mr: 2, fontSize:40 }} />
                  <Typography variant="h4" color="inherit" noWrap>
                      Carte du restaurant
                  </Typography>
                  </Toolbar>
              </AppBar>
                  <Carte/>
                  <Stack
              sx={{ pt: 6, color: '#fff' }}
              direction="row"
              spacing={2}
              justifyContent="center">
              <Button href="/reservation" variant="contained" color='tertiary' size='large'>Reserver une Table</Button>
            </Stack>
                <Footer/>
            </ThemeProvider>
        </React.Fragment>
    )
}

export default CartePage