import '../styles/PlatItem.css'

function PlatItem({ id, cover, name}) {
    return (
        <li key={id} className='lmj-plat-item'>
            <img className='lmj-plat-item-cover' src={cover} alt={`${name} cover`} />
            {name}
        </li>
    )
}

export default PlatItem