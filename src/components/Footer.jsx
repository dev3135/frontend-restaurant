import * as React from 'react';
import { Box, Link, Typography } from "@mui/material"
import CallSharpIcon from '@mui/icons-material/CallSharp';
import HorairesList from './HorairesList';
import { ThemeProvider } from '@mui/material/styles';
import { createTheme, CssBaseline } from '@mui/material';
import Button from '@mui/material/Button';

const theme = createTheme({
  palette: {
    primary: {
      main: '#B6AC97',
    },
    secondary: {
      main: '#392C1E',
    },
    tertiary: {
      main: '#906427',
    }
  },
});

function Copyright() {
    return (
      <Typography variant="body2" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://mui.com/">
          Quai Antique
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }
  
function Footer(){
    return(
      <React.Fragment>
      <ThemeProvider theme={theme}>
          <CssBaseline />
        <Box sx={{ color: '#fff', bgcolor: '#392C1E' , mt: 5 }} component="footer">
            <Box sx={{ display: 'flex', alignItems: 'center', m: 4, justifyContent: 'space-between' }}> 

                      <Box>
                          <HorairesList/>
                      </Box>

                      <Box>
                          <Typography sx={{ color: '#fff'}} variant="H6" align="center" gutterBottom component="p">
                        A PROPOS 
                        </Typography>
                        <Typography
                        variant="subtitle1"
                        align="center"
                        component="p"
                        >
                        Mention legales
                        </Typography>
                        <Typography
                        variant="subtitle1"
                        align="center"
                        component="p"
                        >
                        Politique de Vente
                        </Typography>
                        <Typography
                        variant="subtitle1"
                        align="center"
                        component="p"
                        >
                        Politique de confidentialité
                        </Typography>
                        <Copyright />
                        <Typography
                        variant="subtitle1"
                        align="left"
                        component="p"
                        >
                        </Typography>
                          <Button sx={{ m: 2 }} target="_blank" href="http://quai-antique-alex.herokuapp.com/" variant="contained" color='tertiary' size='large'>Espace gestionnaire</Button>
                      </Box>

                      <Box>
                            <Typography variant="h6" align="right" gutterBottom>
                        CONTACTEZ-NOUS
                          </Typography>
                          <Typography variant="subtitle1" align="right" component="p">
                            <CallSharpIcon sx={{ mr: 2 }} />
                              01.34.50.62.35
                            </Typography>
                          <Typography variant="subtitle1" align="right" component="p">
                          10 Avenue d'albigny 74000 ANNECY
                          </Typography>
                      </Box>       
            </Box> 
        </Box>
        </ThemeProvider>
        </React.Fragment>
 )
}

export default Footer