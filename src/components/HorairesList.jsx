import * as React from 'react';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import axios from '../api/axios';
import { useEffect, useState } from 'react';
import Alert from '@mui/material/Alert';
 
const theme = createTheme({
  palette: {
    primary: {
      main: '#B6AC97',
    },
    secondary: {
      main: '#392C1E',
    },
    tertiary: {
      main: '#906427',
    }
  },
});

const URL_HORAIRES = '/api/horaires';

function HorairesList() {
  const [horaires, setHoraires] = useState([]);
  const [errMsg, setErrMsg] = useState();

  useEffect(() => {
    setErrMsg('');
      async function getHoraires(){
        try {
          const response = await axios.get(
            URL_HORAIRES,
          );

          // Traitement Regroupement des horaires par jours
          const groupedHoraires = response.data.reduce((acc, horaire) => {
            const jourTitre = horaire.jour.titre;
            const horaireGroup = acc.find(group => group.jour === jourTitre);
            if (horaireGroup) {
              horaireGroup.horaires.push(horaire);
            } else {
              acc.push({ jour: jourTitre, horaires: [horaire] });
            }
            return acc;
          }, []);
           // Fin du Traitement Regroupement des horaires par jours
          setHoraires(groupedHoraires);
        }
        catch (err) {
            if (!err?.response) {
              setErrMsg('Aucune réponse du serveur');
          } else if (err.response?.status === 401) {
              setErrMsg('Accès non autorisé');
          } else {
              setErrMsg('Echec de la mise à jour');
          }
        }
      }
    getHoraires();
  }, []);

  return (
      <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container component="main" maxWidth="md">
      <Box sx={{ display: 'flex', alignItems: 'center', m: 4}}>
            {errMsg && (
                <Alert severity="warning">{errMsg}</Alert>
            )} 
      </Box>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
        }}
        >
          <Box sx={{ fontWeight: 'bold'}}>Horaires d'ouverture</Box>
      </Box>
      {horaires.map((row) => (
        <Box key={row.jour} sx={{ display: 'flex',
            justifyContent: 'left',
            justifyContent: 'space-between',
            }}>

            <Box sx={{ textTransform: 'uppercase', fontWeight: 'bold', fontSize:'12px', p: 0.5, m: 0.5 }}>{row.jour}</Box>
          
            <Box sx={{textTransform: 'uppercase', fontWeight: 'bold', fontSize:'12px', p: 0.5, m: 0.5 }}>
              {row.horaires.map(horaire => (
                    <Box key={horaire.id}>
                      {horaire.heureDebut != '' && (
                        <Box key={horaire.id}>
                            {new Date(horaire.heureDebut).toLocaleTimeString("fr-FR", { hour: '2-digit', minute: '2-digit', hour12: false })} 
                            - 
                            {new Date(horaire.heureFin).toLocaleTimeString("fr-FR", { hour: '2-digit', minute: '2-digit', hour12: false })} 
                        </Box>
                      )} 
                      {horaire.heureDebut == '' || horaire.heureDebut == null && (
                        <Box key={horaire.id}>Fermé</Box>
                      )}
                    </Box>
              ))}
            </Box>
        </Box>
      ))}
      </Container>
   </ThemeProvider>
  );
}
          
export default HorairesList