import * as React from 'react';
import { useRef, useState, useEffect, useContext } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import useToken from './userToken';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import useAccount from './userAccount';
import TextField from '@mui/material/TextField';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import axios from '../api/axios';
import Alert from '@mui/material/Alert';
import Header from './Header';
import { AppBar, Toolbar, Typography } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Footer from './Footer';

const theme = createTheme({
    palette: {
      primary: {
        main: '#B6AC97',
      },
      secondary: {
        main: '#392C1E',
      },
      secondary2: {
        main: '#906427',
      }
    },
});

const USER_URL = '/api/users';

const Account = ({ setAccount }) => {
    const { token, setToken } = useToken();
    const { account } = useAccount();
    const [error, setError] = useState(false)
    const [errMsg, setErrMsg] = useState('');
    const [success, setSuccess] = useState(false)

    const [allergie, setAllergie] = useState(account.allergie);

    const handleChange = (event) => {
      setAllergie(event.target.checked);
    };
    const handleClose = () => {
      // On vide le token d'authentification
      setToken({});
      // On vide le compte 
      setAccount({});
      window.location.replace("/");
    };
    const handleSubmit = async (event) => {
      event.preventDefault();

      try {
        const data = new FormData(event.currentTarget);
        const nbCouvert = parseInt(data.get('nbCouvert'));
        const headers = { 
          'Authorization': `Bearer ${token}`
        }
        const response = await axios.put(
          `${USER_URL}/${account.id}`,
            JSON.stringify({ nbCouvert, allergie }),
            {
              headers,
            }
        );
        if(response){
          setError(false)
          setSuccess(true);
        }
      } catch (err) {
          if (!err?.response) {
              setErrMsg('Aucune réponse du serveur');
          } else if (err.response?.status === 401) {
              setErrMsg('Accès non autorisé');
          } else {
              setErrMsg('Echec de la mise à jour');
          }
          setError(true)
          setSuccess(false);
        }
    };  
    return(
        <React.Fragment>
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Header/>
            <AppBar position="relative">
            <Toolbar>
            <AccountCircleIcon sx={{ mr: 2, fontSize:40}} />
            <Typography variant="h4" color="inherit" noWrap>
              Mon profil
            </Typography>
            </Toolbar>
            </AppBar>
            <Container maxWidth="lg">
              <Box sx={{
                      marginTop: 8,
                      display: 'flex',
                      justifyContent: 'space-between'
                  }}>
              </Box>
                    <Box component="form" onSubmit={handleSubmit} noValidate
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                        }}>
                        <p>Mon compte</p> 
                        <TextField margin="normal" required fullWidth id="email" label="Addresse email" name="email"
                            autoComplete="email"
                            autoFocus
                            disabled
                            defaultValue={account.email}
                        />
                        <TextField margin="normal" required fullWidth id="nom" label="Nom"name="nom" autoComplete="nom"
                            autoFocus
                            defaultValue={account.nom}
                        />
                        <TextField margin="normal" required fullWidth id="prenom" label="Prénom"name="prenom"
                            autoComplete="prenom"
                            autoFocus
                            defaultValue={account.prenom}
                        />
                       <TextField margin="normal" required fullWidth id="nbCouvert" label="Nombre de couvert" name="nbCouvert"
                            autoComplete="nbCouvert"
                            autoFocus
                            defaultValue={account.nbCouvert}
                        />
                       <FormControlLabel control={<Checkbox
                            name="allergie"
                            defaultValue={account.allergie}
                            checked={allergie}
                            onChange={handleChange}
                            inputProps={{ 'aria-label': 'controlled' }}
                          />} label="Précautions allergies" />
                 
                          {error && (
                            <Alert severity="warning">{errMsg}</Alert>
                          )}
                          {success && (
                              <Alert severity="success">Sauvegarde réussie</Alert>
                          )}
                        <Box sx={{
                            marginTop: 8,
                            display: 'flex',
                            justifyContent: 'space-between'
                        }}>
                      <Button type="submit"variant="contained"> 
                        Sauvegarder mon profil
                      </Button>
                      <Button onClick={handleClose} variant="contained" color='secondary' size='large'>Se deconnecter</Button>
                    </Box>
                    
                  </Box>
           </Container>
         <Footer/>
        </ThemeProvider>
    </React.Fragment>

    )
}            

export default Account