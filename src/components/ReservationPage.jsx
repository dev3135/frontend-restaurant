import * as React from 'react';
import Reservation from './Reservation'
import Footer from './Footer'
import Header from './Header'
import {  AppBar, Toolbar, Typography } from "@mui/material"
import CalendarMonthSharpIcon from '@mui/icons-material/CalendarMonthSharp';

import { ThemeProvider } from '@mui/material/styles';
import { createTheme, CssBaseline } from '@mui/material';

const theme = createTheme({
  palette: {
    primary: {
      main: '#B6AC97',
    },
    secondary: {
      main: '#392C1E',
    },
    secondary2: {
      main: '#906427',
    }
  },
});

function ReservationPage() {
  return (
    <React.Fragment>
         <ThemeProvider theme={theme}>
          <CssBaseline />
          <Header/>
            <AppBar position="relative">
              <Toolbar>
                <CalendarMonthSharpIcon sx={{ mr: 2 }} />
                <Typography variant="h4" color="inherit" noWrap>
                  Reserver une table
                </Typography>
              </Toolbar>
            </AppBar>
            <Reservation/>
          <Footer/>
        </ThemeProvider>
    </React.Fragment>
  )
}

export default ReservationPage
