import { useState } from 'react';

export default function useAccount() {
  const getAccount = () => {
    const accountData = JSON.parse(localStorage.getItem('account'));
    return accountData?.account;
  };

  const [account, setAccount] = useState(getAccount());

  const saveAccount = accountData => {
    localStorage.setItem('account', JSON.stringify(accountData));
    setAccount(accountData.account);
  };

  return {
    setAccount: saveAccount,
    account
  }
}