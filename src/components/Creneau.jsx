import * as React from 'react';
import Button from '@mui/material/Button';
import { Box } from '@mui/system';
import axios from '../api/axios';
import { useEffect, useState } from 'react';
import Alert from '@mui/material/Alert';

const URL_HORAIRES = '/api/horaires';

function Creneau(props) {

  const {jour, onHoraireChange, onDataChange} = props;
  const [selectedHoraire, setSelectedHoraire] = useState(null);
  const [horaires, setHoraires] = useState([]);
  const [errMsg, setErrMsg] = useState();

  function handleHoraireClick(heureDebut) {
    setSelectedHoraire(heureDebut);
    onDataChange(heureDebut);
    onHoraireChange(false);
  }

  useEffect(() => {
    setErrMsg('');
      async function getCreneaux(){
        try {
          const response = await axios.get(
            URL_HORAIRES,
          );
          setHoraires(response.data);
        }
        catch (err) {
            if (!err?.response) {
                setErrMsg('No Server Response');
            }
            else if (err.response?.status === 401) {
                setErrMsg('Unauthorized');
            } else {
                setErrMsg('Echec création réservation');
            }
        }
      }
    getCreneaux();
  }, []);

    // Création de la liste complète de tous les horaires par tranche de 15 minutes
    const horairesComplets = horaires.filter((horaire) => horaire.jour.titre == jour) 
    .reduce((acc, horaire) => {
      const debut = new Date(horaire.heureDebut);
      const fin = new Date(horaire.heureFin);
      while (debut < fin) {
        const prochaineTranche = new Date(debut.getTime() + 15 * 60000); // Ajout de 15 minutes
        acc.push({
          id: horaire.id,
          jour: horaire.jour,
          isMidi: horaire.isMidi,
          heureDebut: new Date(debut.getTime()).toLocaleTimeString("fr-FR", { hour: '2-digit', minute: '2-digit', hour12: false }),
          heureFin: prochaineTranche.toLocaleTimeString("fr-FR", { hour: '2-digit', minute: '2-digit', hour12: false }),
        });
        debut.setTime(prochaineTranche.getTime());
      }
      return acc;
    }, []);

  // Filtrage des horaires du midi et du soir
  const horairesMidi = horairesComplets.filter((horaire) => horaire.isMidi);
  const horairesSoir = horairesComplets.filter((horaire) => !horaire.isMidi);

  return (
    <React.Fragment>
      <Box sx={{ display: 'flex', alignItems: 'center', m: 4}}>
      {errMsg && (
          <Alert severity="warning">{errMsg}</Alert>
      )} 
      </Box>
      <h2>Horaires du midi</h2>
      <Box sx={{ display: 'flex', flexDirection: 'row', m: 0.5, flexWrap: 'wrap' }}>
        {/* {horairesMidi == null && (
          <Box sx={{ display: 'flex', alignItems: 'center', m: 4}}>
            <Alert severity="warning">Aucun créneau sur la date choisie</Alert>
          </Box>
        )} */}
        {horairesMidi.map((horaire, index) => (
          <Box key={index} sx={{ m: 0.5 }} >
              <Button key={index} onClick={() => handleHoraireClick(horaire.heureDebut)}
                  color={selectedHoraire === horaire.heureDebut ? "success" : "secondary"} variant="contained">
                  {horaire.heureDebut}
                </Button>
          </Box>
        ))}
      </Box>
      <h2>Horaires du soir</h2>
      <Box sx={{ display: 'flex', flexDirection: 'row', m: 0.5, flexWrap: 'wrap' }}>
        {/* {horairesSoir == null && (
            <Box sx={{ display: 'flex', alignItems: 'center', m: 4}}>
              <Alert severity="warning">Aucun créneau sur la date choisie</Alert>
            </Box>
        )} */}
        {horairesSoir.map((horaire, index) => (
            <Box key={index} sx={{ m: 0.5 }}>
              <Button key={index} onClick={() => handleHoraireClick(horaire.heureDebut)}
                  color={selectedHoraire === horaire.heureDebut ? "success" : "secondary"} variant="contained">
                  {horaire.heureDebut}
                </Button>
            </Box>
        ))}
        </Box>
    </React.Fragment>
  );
    
}
export default Creneau;
