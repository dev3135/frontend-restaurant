import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { galerieList } from '../datas/galerieList';

function Galerie() {
  return (
    <React.Fragment>
      <AppBar position="relative">
        <Toolbar>
          <CameraIcon sx={{ mr: 2 }} />
          <Typography variant="h4" color="inherit" noWrap>
            Plats incontournables
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        <Box
          sx={{
            bgcolor: 'background.paper',
            pt: 6,
            pb: 6,
          }}
        >
          <Container maxWidth="m">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Nos meilleurs plats
            </Typography>
            <Typography variant="h5" align="center" color="text.secondary" paragraph>
              Vous trouverez un Album de nos Plats les plus apprécié par nos convives.
            </Typography>
          </Container>
        </Box>
        <Container maxWidth="md">
          {/* End hero unit */}
          <Grid container spacing={4}>
            {galerieList.map((elementDuTableau) => (
              <Grid item key={elementDuTableau.id} xs={12} sm={6} md={4}>
                <Card sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                  <CardMedia component="img"
                    image = {elementDuTableau.image}
                    alt={elementDuTableau.titre}/>
                </Card>
              </Grid>
            ))}
          </Grid>
          <Stack
              sx={{ pt: 6, color: '#fff' }}
              direction="row"
              spacing={2}
              justifyContent="center">
              <Button href="/reservation" variant="contained" color='tertiary' size='large'>Reserver une Table</Button>
            </Stack>
        </Container>
      </main>
    </React.Fragment>
  );
 }
export default Galerie