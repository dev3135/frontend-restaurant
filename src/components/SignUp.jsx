import React, { useState } from 'react';
import { AppBar, Button, FormControlLabel, TextField, Toolbar, Typography } from '@mui/material';
import { Box, Container } from '@mui/system';
import CssBaseline from '@mui/material/CssBaseline';
import Checkbox from '@mui/material/Checkbox';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import AccountBoxSharpIcon from '@mui/icons-material/AccountBoxSharp';
import Alert from '@mui/material/Alert';
import Header from './Header';
import Footer from './Footer';

const theme = createTheme({
    palette: {
      primary: {
        main: '#B6AC97',
      },
      secondary: {
        main: '#392C1E',
      },
      tertiary: {
        main: '#906427',
      }
    },
});

function SignUp() {

    const [errorMdp, setErrorMdp] = useState(false);

    const [formData, setFormData] = useState({
        name: '',
        prenom:'',
        email: '',
        password: '',
        password2: '',
        allergie: '',
    });

    const [allergieChecked, setAllergieChecked] = React.useState(false);

    function handleChange(event) {
        setAllergieChecked(event.target.checked);
        setFormData({
        ...formData,
        [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event) {
        event.preventDefault();

        formData.allergie = allergieChecked;

        if (formData.password !== formData.password2) {
            setErrorMdp(true);
        } else {
        // try {
        //   const response = await axios.post('/api/signup', formData);
        //   console.log(response.data);
        // } catch (error) {
        //   console.error(error);
        // }
        }
    }

  return (
    <React.Fragment>
    <ThemeProvider theme={theme}>
    <CssBaseline />
    <Header/>
    <AppBar position="relative">
            <Toolbar>
            <AccountBoxSharpIcon sx={{ mr: 2 }} />
            <Typography variant="h4" color="inherit" noWrap>
              Inscription
            </Typography>
            </Toolbar>
        </AppBar>
    <Container component="form"
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}>
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', m: 4}}>
            <label htmlFor="name">Nom :</label>
            <TextField type="text" id="name" name="name" value={formData.name} onChange={handleChange} required />
            <label htmlFor="prenom">Prenom :</label>
            <TextField type="text" id="prenom" name="prenom" value={formData.prenom} onChange={handleChange} required />
            <label htmlFor="email" >Email :</label>
            <TextField type="email" id="email" name="email" value={formData.email} onChange={handleChange} required />
            {errorMdp === true && (
                <Alert severity="warning">Mot de passe non identique</Alert>
            )}
            <label htmlFor="password">Mot de passe :</label>
            <TextField type="password" id="password" name="password" value={formData.password} onChange={handleChange} required />
            <label htmlFor="password2">Mot de passe :</label>
            <TextField type="password" id="password2" name="password2" value={formData.password2} onChange={handleChange} required />
            <FormControlLabel sx={{m: 2}} control={<Checkbox
                    name="allergie"
                    id="allergie"
                    value={!allergieChecked}
                    onChange={handleChange}
                    inputProps={{ 'aria-label': 'controlled' }}
                    />} label="Précautions allergies" />
            <Button 
                type="submit"
                variant="contained"
                color='secondary' size='large'
                sx={{ mt: 3, mb: 2 }}
                    >
                    S'inscrire
            </Button>
        </Box>
    </Container>
    <Footer/>
    </ThemeProvider>
    </React.Fragment>

  );
}

export default SignUp