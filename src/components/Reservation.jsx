import * as React from 'react';
import { useState } from 'react';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import dayjs from 'dayjs';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Button from '@mui/material/Button';
import Alert from '@mui/material/Alert';
import Creneau from './Creneau';
import { Box, Container } from '@mui/system';
import axios from '../api/axios';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import useAccount from './userAccount';

const RESERVATION_URL = '/api/reservations';
const RESERVATION_SEUIL = '/api/seuil';
const RESERVATION_SEARCH = '/api/reservations/couverts/search';
function Reservation() {

    const { account } = useAccount();
    const [dateSelected, setValue] = React.useState(null)
    const [errMsg, setErrMsg] = useState('');
    const [heure, setCreneau] = useState(null);
    const [open, setOpen] = React.useState(false);
    const [isFull, setIsFull] = React.useState(false);
    
    const [allergie, setAllergie] = useState();

    let defaultNom = '';
    let defaultNbCouvert = '';

    if(account){
        defaultNom = account.nom;
        defaultNbCouvert = account.nbCouvert;
        setAllergie(account.allergie);
    } 

    const handleChange = (event) => {
      setAllergie(event.target.checked);
    };

    const handleClose = () => {
        setOpen(false);
        // On redirige vers la page d'accueil
        window.location.replace("/");
    };

    const handleDataChange = (heureDebut) => {
        setCreneau(heureDebut);
    };
    const handleHoraireChange = (isFull) => {
        setIsFull(isFull);
    };


    // Récupère le jour en type texte ex: lundi.
    const jour = new Date(dateSelected).toLocaleString('fr-FR', { weekday: 'long' });
    // On passe au format En pour enregistré dans la base de données sans faire de conversion
    const dateFr = new Date(dateSelected).toLocaleDateString('fr');
    // Date au format 2023-03-15 pour la base de données
    const date = dateFr.split('/').reverse().join('-')
    // -----Début  Date picker paramètres ------ 
    const today = dayjs();
    // ----- Fin Date picker paramètres ------   

    const handleSubmit = async (event) => {
        event.preventDefault();
        
        // On récupère les données du formulaires
        const data = new FormData(event.currentTarget);

        // On stock chaque données dans des variables
        const nbCouvert = parseInt(data.get('nbCouvert'));
        const nom = data.get('nom');
        const allergie = data.get('allergie') === "on" ? true : false;

        // Vérifie les données du formulaire
        if(nom == ''){
            setErrMsg('Veuillez préciser un nom');
        }
        else if(nbCouvert <= 0){
            setErrMsg('Veuillez préciser le nombre de convives');
        }
        // Si aucune heure n'est sélectionnée la valeur est null
        else if(heure === null){
            setErrMsg('Veuillez préciser une date de réservation et une heure');
        } else {
            const responseSeuil = await axios.get(
                RESERVATION_SEUIL,
            );
            const seuil = responseSeuil.data.seuilConvive;
            const responseNbCouvert = await axios.get(
                `${RESERVATION_SEARCH}?date=${date}&heure=${heure}`,
            );
            const nbCouvertReserve = responseNbCouvert.data.nbCouvert;
            if(nbCouvertReserve >= seuil){
                setIsFull(true);
            } else {
                try {
                    const response = await axios.post(
                        RESERVATION_URL,
                        JSON.stringify({ nbCouvert, allergie, nom, date, heure }),
                        {
                            headers: { 'Content-Type': 'application/json' },
                        }
                    );
                    if(response.status === 201){
                        setOpen(true);
                    }
                }
                catch (err) {
                    if (!err?.response) {
                        setErrMsg('No Server Response');
                    }
                    else if (err.response?.status === 401) {
                        setErrMsg('Unauthorized');
                    } else {
                        setErrMsg('Echec création réservation');
                    }
                }
            }
        }
   };

  return (
    <React.Fragment>
      <Container component="form"
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit}>
         <Box sx={{ display: 'flex', alignItems: 'center', m: 4}}>
            {errMsg && (
                <Alert severity="warning">{errMsg}</Alert>
            )} 
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'center', m: 2}}>
            <TextField id="nom" label="Votre nom" name="nom" variant="outlined" defaultValue={defaultNom}/>
            <TextField sx={{mx: 2}} required type="number" id="nbCouvert" name="nbCouvert" 
            label="Nombre de couverts" variant="outlined" defaultValue={defaultNbCouvert} />
            <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={"en-gb"}>
                <DatePicker id="date" name="date" value={dateSelected} disablePast 
                defaultValue={today} onChange={(newValue) => setValue(newValue)} />
            </LocalizationProvider>
            <FormControlLabel sx={{m: 2}} control={<Checkbox
                name="allergie"
                checked={allergie}
                onChange={handleChange}
                inputProps={{ 'aria-label': 'controlled' }}
                />} label="Précautions allergies" />
        </Box>
        {dateSelected !=null && (
            <Box sx={{ m: 4}}>
                <Creneau jour={jour} onHoraireChange={handleHoraireChange} onDataChange={handleDataChange} />
                {isFull && (
                    <Alert severity="warning">Il n'y a lus de place disponible pour ce créneau</Alert>
                )}
                <Button
                    type="submit"
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Réserver
                </Button>

            </Box>
         )}       
    </Container>
    <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                Réservation
            </DialogTitle>
            <DialogContent>
            <Alert severity="success">
               Votre réservation est <strong>validée!</strong>
            </Alert>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose} autoFocus>
                Fermer
            </Button>
            </DialogActions>
        </Dialog>
    </React.Fragment>
  );
}

export default Reservation