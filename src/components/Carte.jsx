import { Box } from '@mui/system';
import Container from '@mui/material/Container';
import { Typography } from '@mui/material';
import axios from '../api/axios';
import { useEffect, useState } from 'react';
import Alert from '@mui/material/Alert';

const URL_PLATS = '/api/plats';

function Carte() {

    const [plats, setPlats] = useState([]);
    const [errMsg, setErrMsg] = useState();

    useEffect(() => {
        setErrMsg('');
        async function getPlats(){
            try {
            const response = await axios.get(
                URL_PLATS,
            );

            // Traitement Regroupement des plats par categorie
            const groupedPlats = response.data.reduce((acc, plat) => {
            const categorieTitre = plat.categorie.titre;
            const platGroup = acc.find(group => group.categorie === categorieTitre);
            
            if (platGroup) {
                platGroup.plats.push(plat);
            } else {
            acc.push({ categorie: categorieTitre, plats: [plat] });
            }
    
            return acc;
            }, []);
            // Fin du Traitement Regroupement des plats par categorie
            setPlats(groupedPlats);
            }
            catch (err) {
                if (!err?.response) {
                    setErrMsg('Aucune réponse du serveur');
                } else if (err.response?.status === 401) {
                    setErrMsg('Accès non autorisé');
                } else {
                    setErrMsg('Echec de la mise à jour');
                }
            }
        }
        getPlats();
    }, []);

    return (
        <Container component="main">
            <Box sx={{ display: 'flex', alignItems: 'center', m: 4}}>
                {errMsg && (
                    <Alert severity="warning">{errMsg}</Alert>
                )} 
            </Box>
            <Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'left' }}>
                    {plats.map((carte) => (
                        <Box sx={{ bgcolor: "#f7f6f6", borderRadius:"40px", p:2, mb:2, mr:2 }}key={carte.categorie}>
                            <Typography color='secondary' variant="h2" noWrap>
                                {carte.categorie}
                            </Typography>
                            {carte.plats.map((plat) => (
                            <Box sx={{ mt:2 }} key={plat.id}>
                                 <Box sx={{ p:1 }}>
                                     {plat.titre} - {plat.prix} €
                                </Box>
                                <Box sx={{ p:1 }} key={plat.id}>
                                   {plat.description}
                                </Box>
                            </Box>
                            ))}
                        </Box>
                    ))}
                </Box>
         </Container>
    )
}

export default Carte