import axios from 'axios';

const baseUrlApi = process.env.REACT_APP_API_URL

export default axios.create({
    baseURL: baseUrlApi,
});
