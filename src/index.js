import React from 'react';
import ReactDOM from 'react-dom/client';
import './styles/index.css';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import ReservationPage from './components/ReservationPage';
import CartePage from './components/CartePage';
import Login from './components/Login';
import useToken from './components/userToken';
import Home from './components/Home';
import SignUp from './components/SignUp';
import Account from './components/Account';
import useAccount from './components/userAccount';

const root = ReactDOM.createRoot(document.getElementById('root'));

function App() {
  const { account, setAccount} = useAccount();
  const { token, setToken } = useToken();
  return (
    <BrowserRouter>
      <Routes>
          <Route path="/" element={<Home />} />
          <Route path="reservation" element={<ReservationPage />}/>
          <Route path="carte" element={<CartePage/>}/>
          {account && (
            <Route path="mon-compte" element={<Account setAccount={setAccount}/>} />
          )}
          {!account && (
            <Route path="mon-compte" element={<Login setToken={setToken}/>} />
          )}
          {account && (
            <Route path="connexion" element={<Navigate to="/mon-compte" replace />} />
          )}
          {!token && (
            <Route path="connexion" element={<Login setToken={setToken} setAccount={setAccount}/>} />
          )}
          {!token && (
            <Route path="creer-compte" element={<SignUp setToken={setToken}/>} />
          )}
      </Routes>
    </BrowserRouter>
  )
}
root.render(<App />);
