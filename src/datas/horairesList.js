export const horaires = [
    {
      "id": 1,
      "heureDebut": "1970-01-01T12:00:00+01:00",
      "heureFin": "1970-01-01T14:00:00+01:00",
      "isMidi": true,
      "jour": {
        "id": 22,
        "titre": "lundi"
      }
    },
    {
      "id": 2,
      "heureDebut": "1970-01-01T20:00:00+01:00",
      "heureFin": "1970-01-01T22:00:00+01:00",
      "isMidi": false,
      "jour": {
        "id": 22,
        "titre": "lundi"
      }
    },
    {
      "id": 3,
      "heureDebut": "1970-01-01T12:00:00+01:00",
      "heureFin": "1970-01-01T14:00:00+01:00",
      "isMidi": true,
      "jour": {
        "id": 23,
        "titre": "mardi"
      }
    },
    {
      "id": 4,
      "heureDebut": "1970-01-01T20:00:00+01:00",
      "heureFin": "1970-01-01T22:00:00+01:00",
      "isMidi": false,
      "jour": {
        "id": 23,
        "titre": "mardi"
      }
    },
    {
      "id": 5,
      "heureDebut": "1970-01-01T11:00:00+01:00",
      "heureFin": "1970-01-01T15:00:00+01:00",
      "isMidi": true,
      "jour": {
        "id": 24,
        "titre": "mercredi"
      }
    },
    {
        "id": 6,
        "heureDebut": "1970-01-01T19:00:00+01:00",
        "heureFin": "1970-01-01T21:00:00+01:00",
        "isMidi": false,
        "jour": {
          "id": 25,
          "titre": "mercredi"
        }
    },
    {
      "id": 7,
      "heureDebut": "1970-01-01T11:00:00+01:00",
      "heureFin": "1970-01-01T15:00:00+01:00",
      "isMidi": true,
      "jour": {
        "id": 26,
        "titre": "jeudi"
      }
    },
    {
        "id": 8,
        "heureDebut": "",
        "heureFin": "",
        "isMidi": false,
        "jour": {
          "id": 27,
          "titre": "jeudi"
        }
    }
  ];