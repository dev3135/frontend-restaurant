
export const platList = [
        {
            "id": 17,
            "titre": "Lazagne à la bolognaise",
            "description": "description",
            "prix": 12.9,
            "categorie": {
                "id": 17,
                "titre": "Plat"
            },
            "menu": null
        },
        {
            "id": 18,
            "titre": "Moule frite",
            "description": "description",
            "prix": 12.9,
            "categorie": {
                "id": 17,
                "titre": "Plat"
            },
            "menu": {
                "id": 9,
                "titre": "Menu de la Mer",
                "formule": [
                    {
                        "id": 17,
                        "titre": "Formule du midi Mer",
                        "description": "Entrée + Plat ou Plat + Dessert",
                        "prix": 16
                    },
                    {
                        "id": 18,
                        "titre": "Formule du Soir Mer",
                        "description": "Entrée + Plat + Dessert",
                        "prix": 25
                    }
                ]
            }
        },
        {
            "id": 19,
            "titre": "Burger montagnard",
            "description": "description",
            "prix": 14.9,
            "categorie": {
                "id": 14,
                "titre": "Burger"
            },
            "menu": {
                "id": 10,
                "titre": "Menu montagnard",
                "formule": [
                    {
                        "id": 19,
                        "titre": "Formule du midi Montagnard",
                        "description": "Entrée + Plat ou Plat + Dessert",
                        "prix": 14
                    },
                    {
                        "id": 20,
                        "titre": "Formule du Soir Montagnard",
                        "description": "Entrée + Plat + Dessert",
                        "prix": 22
                    }
                ]
            }
        },
        {
            "id": 20,
            "titre": "Crumble au pomme",
            "description": "description",
            "prix": 7.9,
            "categorie": {
                "id": 15,
                "titre": "Dessert"
            },
            "menu": {
                "id": 10,
                "titre": "Menu montagnard",
                "formule": [
                    {
                        "id": 19,
                        "titre": "Formule du midi Montagnard",
                        "description": "Entrée + Plat ou Plat + Dessert",
                        "prix": 14
                    },
                    {
                        "id": 20,
                        "titre": "Formule du Soir Montagnard",
                        "description": "Entrée + Plat + Dessert",
                        "prix": 22
                    }
                ]
            }
        },
        {
            "id": 21,
            "titre": "Créme brûler",
            "description": "description",
            "prix": 6.9,
            "categorie": {
                "id": 15,
                "titre": "Dessert"
            },
            "menu": {
                "id": 9,
                "titre": "Menu de la Mer",
                "formule": [
                    {
                        "id": 17,
                        "titre": "Formule du midi Mer",
                        "description": "Entrée + Plat ou Plat + Dessert",
                        "prix": 16
                    },
                    {
                        "id": 18,
                        "titre": "Formule du Soir Mer",
                        "description": "Entrée + Plat + Dessert",
                        "prix": 25
                    }
                ]
            }
        },
        {
            "id": 22,
            "titre": "Terrine de poisson",
            "description": "description",
            "prix": 10.9,
            "categorie": {
                "id": 16,
                "titre": "Entrée"
            },
            "menu": {
                "id": 9,
                "titre": "Menu de la Mer",
                "formule": [
                    {
                        "id": 17,
                        "titre": "Formule du midi Mer",
                        "description": "Entrée + Plat ou Plat + Dessert",
                        "prix": 16
                    },
                    {
                        "id": 18,
                        "titre": "Formule du Soir Mer",
                        "description": "Entrée + Plat + Dessert",
                        "prix": 25
                    }
                ]
            }
        },
        {
            "id": 23,
            "titre": "Assiete de charcuterie",
            "description": "description",
            "prix": 10.9,
            "categorie": {
                "id": 16,
                "titre": "Entrée"
            },
            "menu": {
                "id": 10,
                "titre": "Menu montagnard",
                "formule": [
                    {
                        "id": 19,
                        "titre": "Formule du midi Montagnard",
                        "description": "Entrée + Plat ou Plat + Dessert",
                        "prix": 14
                    },
                    {
                        "id": 20,
                        "titre": "Formule du Soir Montagnard",
                        "description": "Entrée + Plat + Dessert",
                        "prix": 22
                    }
                ]
            }
        }
    ]


// import burgerMontagnard from '../assets/burgerMontagnard.png'
// import tartiflette from '../assets/tartiflette.png'
// import terrinePoissons from '../assets/terrinePoissons.jpg'
// 
// 
// export const platList = [
    // {
        // name: 'Burger montagnard',
        // category: 'Burger',
        // id: '1ed',
        // cover:burgerMontagnard,
    // },
    // {
        // name: 'Tartiflette',
        // category: 'plat',
        // id: '2ab',
        // cover:tartiflette,
    // },
    // {
        // name: 'Terrine de poisson',
        // category: 'entrée',
        // id: '3sd',
        // cover:terrinePoissons,
    // },
// ]
// 