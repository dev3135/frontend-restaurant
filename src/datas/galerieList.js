export const galerieList = [
    {
        "id": 1,
        "titre": "Image 1",
        "updatedAt": "2023-03-09T16:25:09+01:00",
        "imageFile": null,
        "image": "http://quai-antique-alex.herokuapp.com/uploads/galerie/image-default-6409fa5532b56363153447.webp"
    },
    {
        "id": 2,
        "titre": "Image 2",
        "updatedAt": "2023-03-09T16:25:22+01:00",
        "imageFile": null,
        "image": "http://quai-antique-alex.herokuapp.com/uploads/galerie/image-default-6409fa5532b56363153447.webp"
    },
    {
        "id": 3,
        "titre": "Image 3",
        "updatedAt": "2023-03-09T16:25:36+01:00",
        "imageFile": null,
        "image": "http://quai-antique-alex.herokuapp.com/uploads/galerie/image-default-6409fa70af1bd485441740.webp"
    },
    {
        "id": 4,
        "titre": "Image 4",
        "updatedAt": "2023-03-09T16:25:36+01:00",
        "imageFile": null,
        "image": "http://quai-antique-alex.herokuapp.com/uploads/galerie/image-default-6409fa5532b56363153447.webp"
    },
    {
        "id": 5,
        "titre": "Image 5",
        "updatedAt": "2023-03-09T16:25:36+01:00",
        "imageFile": null,
        "image": "http://quai-antique-alex.herokuapp.com/uploads/galerie/image-default-6409fa5532b56363153447.webp"
    },
]